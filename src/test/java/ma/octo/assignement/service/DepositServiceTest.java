package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.DepositRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class DepositServiceTest {

    @Mock
    private DepositRepository depositRepository;
    @Mock
    private CompteService compteService;
    @Mock
    private AuditService auditService;
    @InjectMocks
    private DepositService depositService;

    private Deposit deposit;
    private DepositDto depositDto;
    private Compte compteBeneficiaire;

    @BeforeEach
    void setUp() {
        compteBeneficiaire = new Compte();
        compteBeneficiaire.setNrCompte("123456789");
        compteBeneficiaire.setRib("RIB");
        compteBeneficiaire.setSolde(new BigDecimal(1000));

        deposit = new Deposit();
        deposit.setNomPrenomEmetteur("Oussama");
        deposit.setCompteBeneficiaire(compteBeneficiaire);
        deposit.setMontant(new BigDecimal(100));
        deposit.setMotifDeposit("Test transfer");
        deposit.setDateExecution(new Date());

        depositDto = new DepositDto();
        depositDto.setNomPrenomEmetteur("Oussama");
        depositDto.setRibBeneficiaire("RIB");
        depositDto.setMontant(new BigDecimal(100));
        depositDto.setMotif("Test transfer");
        depositDto.setDate(new Date());
    }

    @Test
    void shouldCreateDeposit() throws TransactionException, CompteNonExistantException {
        // given
        given(compteService.getCompteByRib("RIB")).willReturn(compteBeneficiaire);
        given(depositRepository.save(deposit)).willReturn(deposit);

        // when
        DepositDto depositDtoCreated = depositService.createTransaction(depositDto);

        // then
        assertEquals(depositDto, depositDtoCreated);
    }

    @Test
    void shouldGetAllDeposits() {
        // given
        given(depositRepository.findAll()).willReturn(List.of(deposit));

        // when
        List<DepositDto> deposits = depositService.getAllDeposits();

        // then
        assertEquals(1, deposits.size());
    }

    @Test
    void createDepositCompteNotExist() {
        depositDto.setRibBeneficiaire("RIB404");

        assertThrows(CompteNonExistantException.class, () -> depositService.createTransaction(depositDto));
    }

    @Test
    void createDepositMontantInValid() {
        given(compteService.getCompteByRib("RIB")).willReturn(compteBeneficiaire);

        depositDto.setMontant(new BigDecimal(1000000));

        assertThrows(TransactionException.class, () -> depositService.createTransaction(depositDto));
    }

}