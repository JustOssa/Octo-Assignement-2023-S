package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class TransferServiceTest {

    @Mock
    private TransferRepository transferRepository;
    @Mock
    private CompteService compteService;
    @Mock
    private AuditService auditService;
    @InjectMocks
    private TransferService transferService;

    private Transfer transfer;
    private TransferDto transferDto;
    private Compte compteEmetteur;
    private Compte compteBeneficiaire;

    @BeforeEach
    void setUp() {
        compteEmetteur = new Compte();
        compteEmetteur.setNrCompte("111");
        compteEmetteur.setRib("RIB111");
        compteEmetteur.setSolde(BigDecimal.valueOf(1000));

        compteBeneficiaire = new Compte();
        compteBeneficiaire.setNrCompte("222");
        compteBeneficiaire.setRib("RIB222");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(1000));

        transfer = new Transfer();
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setMontantTransfer(new BigDecimal(100));
        transfer.setMotifTransfer("Test transfer");
        transfer.setDateExecution(new Date());

        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur("111");
        transferDto.setNrCompteBeneficiaire("222");
        transferDto.setMontant(new BigDecimal(100));
        transferDto.setMotif("Test transfer");
        transferDto.setDate(new Date());
    }

    @Test
    void shouldCreateTransfer() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        // given
        given(compteService.getCompteByNr("111")).willReturn(compteEmetteur);
        given(compteService.getCompteByNr("222")).willReturn(compteBeneficiaire);
        given(transferRepository.save(transfer)).willReturn(transfer);

        // when
        TransferDto transferDtoCreated = transferService.createTransaction(transferDto);

        // then
        assertEquals(transferDto, transferDtoCreated);
    }

    @Test
    void shouldGetAllTransfers() {
        // given
        given(transferRepository.findAll()).willReturn(List.of(transfer));

        // when
        List<TransferDto> transfers = transferService.getAllTransfers();

        // then
        assertEquals(1, transfers.size());
    }

    @Test
    void createTransferCompteNotExist() {
        transferDto.setNrCompteEmetteur("404");
        transferDto.setNrCompteBeneficiaire("405");

        assertThrows(CompteNonExistantException.class, () -> transferService.createTransaction(transferDto));
    }

    @Test
    void createTransferSoldInsuffisant() {
        given(compteService.getCompteByNr("111")).willReturn(compteEmetteur);
        given(compteService.getCompteByNr("222")).willReturn(compteBeneficiaire);

        transferDto.setMontant(new BigDecimal(10000));

        assertThrows(SoldeDisponibleInsuffisantException.class, () -> transferService.createTransaction(transferDto));
    }

    @Test
    void createTransferMontantInValid() {
        given(compteService.getCompteByNr("111")).willReturn(compteEmetteur);
        given(compteService.getCompteByNr("222")).willReturn(compteBeneficiaire);

        transferDto.setMontant(new BigDecimal(1));

        assertThrows(TransactionException.class, () -> transferService.createTransaction(transferDto));
    }


}