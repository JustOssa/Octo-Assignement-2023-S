package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.DepositMapper;
import ma.octo.assignement.repository.DepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class DepositService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);
    private final CompteService compteService;
    private final AuditService auditService;
    private final DepositRepository depositRepository;

    @Autowired
    public DepositService(DepositRepository depositRepository, CompteService compteService, AuditService auditService) {
        this.depositRepository = depositRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }


    public void addDeposit(Deposit deposit) {
        depositRepository.save(deposit);
    }

    public List<DepositDto> getAllDeposits() {
        return DepositMapper.mapAllToDto(depositRepository.findAll());
    }


    public DepositDto createTransaction(DepositDto depositDto) throws CompteNonExistantException, TransactionException {

        Compte compteBeneficiaire = compteService.getCompteByRib(depositDto.getRibBeneficiaire());

        if (compteBeneficiaire == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (depositDto.getMontant() == null || depositDto.getMontant().compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (depositDto.getMontant().compareTo(new BigDecimal(MONTANT_MINIMAL)) < 0) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de dépôt non atteint");
        } else if (depositDto.getMontant().compareTo(new BigDecimal(MONTANT_MAXIMAL)) > 0) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de dépôt dépassé");
        }

        if (depositDto.getMotif() == null || depositDto.getMotif().length() == 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(depositDto.getMontant()));

        Deposit deposit = DepositMapper.mapToEntity(depositDto);
        deposit.setCompteBeneficiaire(compteBeneficiaire);

        auditService.addAudit("dépôt depuis " + depositDto.getNomPrenomEmetteur() + " vers " + depositDto
                .getRibBeneficiaire() + " d'un montant de " + depositDto.getMontant()
                .toString(), EventType.DEPOSIT);

        return DepositMapper.mapToDto(depositRepository.save(deposit));
    }
}
