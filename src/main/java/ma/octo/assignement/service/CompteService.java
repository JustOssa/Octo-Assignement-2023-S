package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompteService {

    private final CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public void addCompte(Compte compte) {
        compteRepository.save(compte);
    }

    public Compte getCompteByNr(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }

    public Compte getCompteByRib(String ribCompte) {
        return compteRepository.findByRib(ribCompte);
    }

    public List<Compte> loadAllComptes() {
        return compteRepository.findAll();
    }


}
