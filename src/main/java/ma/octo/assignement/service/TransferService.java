package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.TransferMapper;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class TransferService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MINIMAL = 10;
    Logger LOGGER = LoggerFactory.getLogger(TransferService.class);
    private final CompteService compteService;
    private final AuditService auditService;
    private final TransferRepository transferRepository;

    @Autowired
    public TransferService(TransferRepository transferRepository, CompteService compteService, AuditService auditService) {
        this.transferRepository = transferRepository;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    public void addTransfer(Transfer transfer) {
        transferRepository.save(transfer);
    }

    public List<TransferDto> getAllTransfers() {
        return TransferMapper.mapAllToDto(transferRepository.findAll());
    }

    public TransferDto createTransaction(TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteService.getCompteByNr(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService.getCompteByNr(transferDto.getNrCompteBeneficiaire());

        if (compteEmetteur == null || compteBeneficiaire == null) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (transferDto.getMontant() == null || transferDto.getMontant().compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().compareTo(new BigDecimal(MONTANT_MINIMAL)) < 0) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().compareTo(new BigDecimal(MONTANT_MAXIMAL)) > 0) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif() == null || transferDto.getMotif().length() == 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().compareTo(transferDto.getMontant()) < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(transferDto.getMontant()));
        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(transferDto.getMontant()));

        Transfer transfer = TransferMapper.mapToEntity(transferDto);
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);

        auditService.addAudit("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString(), EventType.TRANSFER);

        return TransferMapper.mapToDto(transferRepository.save(transfer));
    }
}
