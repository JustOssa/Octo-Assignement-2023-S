package ma.octo.assignement;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransferService;
import ma.octo.assignement.service.UtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.Date;

@SpringBootApplication
public class NiceBankApplication implements CommandLineRunner {

    private final CompteService compteService;
    private final UtilisateurService utilisateurService;
    private final TransferService transferService;

    @Autowired
    public NiceBankApplication(CompteService compteService, UtilisateurService utilisateurService, TransferService transferService) {
        this.compteService = compteService;
        this.utilisateurService = utilisateurService;
        this.transferService = transferService;
    }

    public static void main(String[] args) {
        SpringApplication.run(NiceBankApplication.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        utilisateurService.addUtilisateur(utilisateur1);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");

        utilisateurService.addUtilisateur(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        compteService.addCompte(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        compteService.addCompte(compte2);

        Transfer transfer = new Transfer();
        transfer.setMontantTransfer(BigDecimal.TEN);
        transfer.setCompteBeneficiaire(compte2);
        transfer.setCompteEmetteur(compte1);
        transfer.setDateExecution(new Date());
        transfer.setMotifTransfer("Assignment 2021");

        transferService.addTransfer(transfer);
    }
}
