package ma.octo.assignement.web;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/deposits")
class DepositController {

    private final DepositService depositService;

    @Autowired
    DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @GetMapping
    public List<DepositDto> getAllDeposits() {
        return depositService.getAllDeposits();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DepositDto createTransaction(@RequestBody DepositDto depositDto)
            throws CompteNonExistantException, TransactionException {
        return depositService.createTransaction(depositDto);

    }
}
