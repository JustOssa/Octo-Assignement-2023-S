package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

import java.util.ArrayList;
import java.util.List;

public class TransferMapper {

    public static TransferDto mapToDto(Transfer transfer) {
        TransferDto transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setMontant(transfer.getMontantTransfer());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotif(transfer.getMotifTransfer());
        return transferDto;
    }

    public static Transfer mapToEntity(TransferDto transferDto) {
        Transfer transfer = new Transfer();
        transfer.setDateExecution(transferDto.getDate());
        transfer.setMontantTransfer(transferDto.getMontant());
        transfer.setMotifTransfer(transferDto.getMotif());
        return transfer;
    }

    public static List<TransferDto> mapAllToDto(List<Transfer> transfers) {
        List<TransferDto> transferDtos = new ArrayList<>();
        for (Transfer transfer : transfers) {
            transferDtos.add(mapToDto(transfer));
        }
        return transferDtos;
    }

}
