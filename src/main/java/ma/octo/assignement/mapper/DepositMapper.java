package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.dto.TransferDto;

import java.util.ArrayList;
import java.util.List;

public class DepositMapper {

    public static DepositDto mapToDto(Deposit deposit) {
        DepositDto depositDto = new DepositDto();
        depositDto.setNomPrenomEmetteur(deposit.getNomPrenomEmetteur());
        depositDto.setRibBeneficiaire(deposit.getCompteBeneficiaire().getRib());
        depositDto.setMontant(deposit.getMontant());
        depositDto.setDate(deposit.getDateExecution());
        depositDto.setMotif(deposit.getMotifDeposit());
        return depositDto;
    }

    public static Deposit mapToEntity(DepositDto depositDto) {
        Deposit deposit = new Deposit();
        deposit.setNomPrenomEmetteur(depositDto.getNomPrenomEmetteur());
        deposit.setDateExecution(depositDto.getDate());
        deposit.setMontant(depositDto.getMontant());
        deposit.setMotifDeposit(depositDto.getMotif());
        return deposit;
    }

    public static List<DepositDto> mapAllToDto(List<Deposit> deposits) {
        List<DepositDto> depositDtos = new ArrayList<>();
        for (Deposit deposit : deposits) {
            depositDtos.add(mapToDto(deposit));
        }
        return depositDtos;
    }

}
